from django.db import models


class Contact(models.Model):

    class Meta:
        app_label = 'proj'

    name = models.CharField("Name", max_length=50)
    surname = models.CharField("Surname", max_length=50)
    country = models.CharField("Country", max_length=50)
    city = models.CharField("City", max_length=50)
    street = models.CharField("Street", max_length=50)
    phone = models.CharField("Phone", max_length=50)

    # def __str__(self):
    #     return '{0} {1} - {2}'. format(self.name, self.surname, self.phone)
