import os
from django.http import HttpResponse
from django.shortcuts import render
from proj.models import Contact
from proj.forms import ContactForm
from django.shortcuts import render


def list(request):
    contacts = Contact.objects.all()
    return render(
        request,
        os.path.join("proj", "list.html"),
        {"title": "List Contacts", 'contacts': contacts}
    )


def create(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()

    form = ContactForm()
    return render(
        request,
        os.path.join("proj", "contact.html"),
        {"title": "Create Contact", 'form': form}
    )


def update(request, contact_id=None):
    if request.method == "POST":
        form = ContactForm(request.POST)
        print(form)
        print(form.is_valid())
        if form.is_valid():
            form.save()
            return HttpResponse('Contact was successfully updated')
        return HttpResponse('Failed to update contact')

    contact = Contact.objects.filter(id=contact_id).first()
    form = ContactForm(instance=contact)
    return render(
        request,
        os.path.join("proj", "contact.html"),
        {"title": "Contact Info", 'form': form}
    )


def delete(request, contact_id=None):
    return HttpResponse('DELETE action is not implemented')
