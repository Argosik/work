from django.urls import path

from myfirst.apps.proj import views

urlpatterns = [
    path('', views.list, name='list'),
    path('create', views.create, name='create'),
    path('<int:contact_id>/update', views.update, name='update'),
    path('<int:contact_id>/delete', views.delete, name='delete'),
]
