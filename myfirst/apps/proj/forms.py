from .models import Contact
from django.forms import CharField, ModelForm, TextInput


class ContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = '__all__'
